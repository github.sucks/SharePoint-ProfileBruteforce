# SharePoint Profile Bruteforce
Bruteforce any SharePoint Online website's Profile Pictures.

## You need:
1. An account for the website you are brute-forcing
2. Selenium-wire and Google Webdriver installed
3. Google Chrome or Firefox browser
4. Python 3.x