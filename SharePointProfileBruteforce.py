# Fuck you Mark Zuckerberg, wget doesn't do shit, now I have to code Python.
from seleniumwire import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import urllib
import time 
from urllib.parse import urlparse
import os
from mimetypes import guess_extension

# Settings
driver = webdriver.Chrome() # Change this to Firefox() if you want to
user = "1205426@me.sd42.ca"
passwd = "lk2009.07.29"
filelist = open('list.txt') # Change this to your own list file if you want to

l = filelist.readline()

asset_dir = "" # Credit for the Download method: Paco
def download_assets(requests,
                   asset_dir="",
                   exts=[".jpg"],
                   append_ext=False):
    asset_list = {}
    for req_idx, request in enumerate(requests):
        if request is None or request.response is None or request.response.headers is None or 'Content-Type' not in request.response.headers:
            continue
            
        ext = guess_extension(request.response.headers['Content-Type'].split(';')[0].strip())
        if ext is None or ext == "" or ext not in exts:
            #Don't know the file extention, or not in the whitelist
            continue
        parsed_url = urlparse(request.url)
        frelpath = parsed_url.path.strip()
        if frelpath.startswith("\\") or frelpath.startswith("/"):
            frelpath = frelpath[1:]
        
        fpath = os.path.join(asset_dir, parsed_url.netloc, frelpath)
        if os.path.isfile(fpath):
            continue
        os.makedirs(os.path.dirname(fpath), exist_ok=True)
        print(f"Downloaded: {request.url}")
        asset_list[fpath] = request.url
        try:
            with open(fpath, "wb") as file:
                file.write(request.response.body)
        except:
            print(f"Cannot download: {request.url} to {fpath}")
    return asset_list

# URL/Image/Login Settings, use "{l}" in your URL for where you want the user list to go
driver.get('https://fs.sd42.ca/adfs/ls?wa=wsignin1.0&wtrealm=urn%3asharepoint%3aportal&wctx=https%3a%2f%2fportal.sd42.ca%2fmy%2f_layouts%2f15%2fAuthenticate.aspx%3fSource%3d%252Fmy%252FPages%252FHub%252Easpx')
driver.find_element("id", "userNameInput").send_keys(user)
driver.find_element("id", "passwordInput").send_keys(passwd)
driver.find_element("id", "submitButton").click()
time.sleep(3)

#for number in range(1000000, 2900000): (do without list and brute-force numbers going up)
while l!= "":
    driver.get(f'https://my.sd42.ca/_layouts/15/Authenticate.aspx?Source=%2FUser%20Photos%2FProfile%20Pictures%2Fi%5F0e%2Et%5Fadfs3%5F{l}%40mrpm%2Esd42%2Eca%5FLThumb%2Ejpg')
    download_assets(driver.requests, asset_dir=asset_dir)
    l = filelist.readline()